import React, { useState } from "react";
import Email from "./components/EmailAdress";
import Quiz from "./components/Quiz";
import Result from "./components/Result";
import logo from "./static/logo.png";
import "./App.scss";

const App = () => {
  const [state, setState] = useState({
    currentPage: "login",
    score: 0
  });

  const changeCurrentPage = (currentPage, score = 0) =>
    setState({ currentPage, score });

  const { currentPage, score } = state;
  return (
    <div className="App">
      <header className="navbar">
        <a rel="noopener noreferrer" target="_blank" href="https://x-hub.io/">
          <img src={logo} className="App-logo" alt="logo" />
        </a>
        <span className="header-text"> CodeIT powered by X-HUB</span>
      </header>

      {currentPage === "login" && (
        <Email changeCurrentPage={changeCurrentPage} />
      )}

      {currentPage === "quiz" && <Quiz changeCurrentPage={changeCurrentPage} />}

      {currentPage === "result" && (
        <div id="container">
          <Result score={score} />
        </div>
      )}
    </div>
  );
};

export default App;
