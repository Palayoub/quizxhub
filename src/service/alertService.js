import swal from "sweetalert";

export const scoreAlert = score =>
  swal("Good Job you finished!", `your score is ${score}`, "success");
export const warningAlert = msg => swal("7adaarii", msg, "warning");
