export const saveEmail = email =>
  localStorage.setItem("email", JSON.stringify(email));
export const getEmail = () => localStorage.getItem("email");

export const postResults = ({ url, results }) =>
  fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    // mode: "no-cors", // no-cors, cors, *same-origin
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(results) // body data type must match "Content-Type" header
  }).then(res => res.json());
