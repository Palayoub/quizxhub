import { warningAlert } from "./alertService";

import { postResults, getEmail } from "./persistance";

export const calculateScore = (data, hisAnswers) => {
  let score = 0;
  data.forEach(({ question, correctAnswer, answers }) => {
    const res = hisAnswers.filter(hisAnswer => hisAnswer[question])[0];
    //console.log(res[question], correctAnswer);
    if (res[question] === answers[correctAnswer]) {
      // console.log(res[question] === correctAnswer);
      score += 1;
    }
  });

  return (score / data.length) * 100;
};

export const processAnswers = async (data, answers, push) => {
  if (answers.length !== data.length) {
    warningAlert("sir kml test awlidi lyrdi 3lik!");
    return;
  } else {
    const score = calculateScore(data, answers);
    //scoreAlert(`score is : ${score}%`);
    const email = getEmail();
    await postResults({
      url: "http://xhub.ddns.net:4000/api/users",
      results: {
        email,
        data: {
          score,
          answers
        }
      }
    });

    push("result", score.toFixed(2));

    //console.log(res);
  }
};
