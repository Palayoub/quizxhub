export default [
  {
    question: "bla_1",
    correctAnswer: "foo",
    answers: ["foo", "bar", "baz"]
  },
  {
    question: "bla_2",
    correctAnswer: "foo",
    answers: ["foo", "bar", "baz"]
  },
  {
    question: "bla_3",
    correctAnswer: "foo",
    answers: ["foo", "bar", "baz"]
  },
  {
    question: "bla_4",
    correctAnswer: "foo",
    answers: ["foo", "bar", "baz"]
  },
  {
    question: "bla_5",
    correctAnswer: "foo",
    answers: ["foo", "bar", "baz"]
  }
];
