import React from "react";

const Result = ({ score }) => {
  if (score >= 30) {
    return (
      <div id="success-box">
        <div className="face">
          <div className="eye" />
          <div className="eye right" />
          <div className="mouth happy" />
        </div>
        <div className="shadow scale" />
        <div className="message">
          <h1 className="alert">your scrore is {score}%</h1>
        </div>
      </div>
    );
  } else
    return (
      <div id="error-box">
        <div className="face2">
          <div className="eye" />
          <div className="eye right" />
          <div className="mouth sad" />
        </div>
        <div className="shadow move" />
        <div className="message">
          <h1 className="alert">your score is {score}%</h1>
        </div>
      </div>
    );
};

export default Result;
