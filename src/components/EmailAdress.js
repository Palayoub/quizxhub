import React, { useState } from "react";
import { saveEmail } from "../service/persistance";

const EmailComponent = ({ changeCurrentPage }) => {
  const [email, setEmail] = useState("");
  return (
    <div className="sign-in">
      <form
        onSubmit={() => {
          //alert(email);
          saveEmail(email);
          changeCurrentPage("quiz");
        }}
      >
        <h3>Please enter your email address</h3>
        <input
          className="form-control"
          type="email"
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
        <input type="submit" />
      </form>
    </div>
  );
};

export default EmailComponent;
