import React, { useState } from "react";
import { processAnswers } from "../service/scoreProcessing";
import { getDistinctQuestions } from "../service/getQuestions";

let data = require("../data/data.json");

data = getDistinctQuestions(data, 15);

//console.log(data);

const useQuiz = () => {
  const [answers, setAnswers] = useState([]);
  function addAnswer({ question, answer }) {
    const filteredAnswers = answers.filter(myAnswer => {
      //console.log(myAnswer[question], !myAnswer[question]);
      return !myAnswer[question];
    });

    const newsAnswers = [...filteredAnswers, { [question]: answer }];
    //console.log(newsAnswers);

    setAnswers(newsAnswers);
  }

  return [answers, addAnswer];
};

const Questions = ({ changeCurrentPage }) => {
  const [hisAnswers, addAnswer] = useQuiz();

  //console.log(hisAnswers);
  return (
    <div className="quiz">
      {data.map(({ question, answers }, qIndex) => {
        return (
          <div className="question-item" key={`question${qIndex}`}>
            <h3>{question}</h3>
            <ul>
              {answers.map((an, anIndex) => (
                <li key={`key${anIndex}`}>
                  <input
                    type="radio"
                    name={`question${qIndex}`}
                    id={`anwser-${qIndex}-${anIndex}`}
                    onChange={() => addAnswer({ question, answer: an })}
                  />
                  <label htmlFor={`anwser-${qIndex}-${anIndex}`}>{an}</label>
                </li>
              ))}
            </ul>
          </div>
        );
      })}
      <SubmitQcm
        handleSubmit={() => {
          processAnswers(data, hisAnswers, changeCurrentPage);
        }}
      />
    </div>
  );
};

const SubmitQcm = ({ handleSubmit }) => (
  <button className="submit" onClick={handleSubmit}>
    submit
  </button>
);
export default Questions;
